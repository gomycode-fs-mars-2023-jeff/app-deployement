import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Dashboard from './pages/Dashboard';
import Filiere from './pages/Filiere';
import Matiere from './pages/Matiere';
import Prof from './pages/Prof';
import Coeff from './pages/Coeff';
import Note from './pages/Note';
import Students from './pages/Students';
import Sidebar from './components/Sidebar';
import './components/Sidebar.css';
import './components/Dashboard.css';
import './components/Students.css'

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Sidebar>
          <Routes>
            <Route path="/" element={<Dashboard />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/students" element={<Students />} />
            <Route path="/prof" element={<Prof />} />
            <Route path="/coeff" element={<Coeff />} />
            <Route path="/note" element={<Note />} />
            <Route path="/matiere" element={<Matiere />} />
            <Route path="/filiere" element={<Filiere />} />
          </Routes>
        </Sidebar>
        </BrowserRouter>
    </div>
  );
}

export default App;
