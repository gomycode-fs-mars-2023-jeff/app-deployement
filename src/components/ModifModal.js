import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { updateStudent } from '../reduxs/students/StudentsSlice';
import "./Modal.css"


const ModifModal = ({ student }) => {
  const dispatch = useDispatch();
  const [nom, setNom] = useState(student.nom);
  const [prenom, setPrenom] = useState(student.prenom);
  const [matricule, setMatricule] = useState(student.matricule);
  const [sexe, setSexe] = useState(student.sexe);
  const [age, setAge] = useState(student.age);

  const handleSubmit = () => {
    const updatedStudent = {
      id: student.id,
      nom,
      prenom,
      matricule,
      sexe,
      age,
    };
    dispatch(updateStudent(updatedStudent));

  };

  return (
    <div className='modal-container'>
      <div className='modal'>
        <h2>Modifier l'étudiant</h2>
        <form>
          <div className='form-group'>
            <label>Nom</label>
            <input type="text" value={nom} onChange={(e) => setNom(e.target.value)} />
          </div>
          <div className='form-group'>
            <label>Prénom</label>
            <input type="text" value={prenom} onChange={(e) => setPrenom(e.target.value)} />
          </div>
          <div className='form-group'>
            <label>Matricule</label>
            <input type="text" value={matricule} onChange={(e) => setMatricule(e.target.value)} />
          </div>
          <div className='form-group'>
            <label>Sexe</label>
            <select value={sexe} onChange={(e) => setSexe(e.target.value)}>
              <option value="Homme">Homme</option>
              <option value="Femme">Femme</option>
            </select>
          </div>
          <div className='form-group'>
            <label>Âge</label>
            <input type="number" value={age} onChange={(e) => setAge(e.target.value)} />
          </div>
          <button className='btn' type="button" onClick={handleSubmit}>Enregistrer</button>
        </form>
      </div>
    </div>
  );
};

export default ModifModal;
