import React from 'react';
import { FaChalkboardTeacher, FaUserGraduate, FaSchool, FaBook } from 'react-icons/fa';
import { useSelector } from 'react-redux';


const Dashboard = () => {
    const students = useSelector(state => state.student);
    return (
        <div>
            <div className="title"><h1>Tableau de bord</h1></div>
            <div className="container">
                <div className="cards">
                    <div className="card">
                        <div className="card-content">
                            <div className="number">{students.length}</div>
                            <div className="card-name">Elèves</div>
                        </div>
                        <div className="icon-box">
                            <FaUserGraduate className='icon'/>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-content">
                            <div className="number">1200</div>
                            <div className="card-name">Professeurs</div>
                        </div>
                        <div className="icon-box">
                            <FaChalkboardTeacher className='icon'/>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-content">
                            <div className="number">1200</div>
                            <div className="card-name">Filières</div>
                        </div>
                        <div className="icon-box">
                            <FaSchool className='icon'/>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-content">
                            <div className="number">1200</div>
                            <div className="card-name">Matières</div>
                        </div>
                        <div className="icon-box">
                            <FaBook className='icon'/>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    );
};

export default Dashboard;