import { configureStore } from '@reduxjs/toolkit'
import { StudentsSlice }  from "./students/StudentsSlice"


export const store = configureStore({
    reducer: {
        student: StudentsSlice.reducer,
    }
})
